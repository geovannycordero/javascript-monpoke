const fs = require('fs');

const game = require('./src/game');

const commandsFilePath = process.argv[2] || '';
if (commandsFilePath === '') {
  console.error('🤔 Oh no, you forgot the filepath for input 🤔\n'); // eslint-disable-line no-console
  process.exit(1);
}
const commands = fs.readFileSync(commandsFilePath, 'utf8', (err, data) => {
  if (err) throw err;
  return data;
});

console.log(game(commands)); // eslint-disable-line no-console
