const attackEnemy = require('./attackEnemy');
const chooseFighter = require('./chooseFighter');
const createMonpokeOnTeam = require('./createMonpokeOnTeam');
const error = require('./error');
const healFighter = require('./healFighter');
const reviveFighter = require('./reviveFighter');

module.exports = commandText => {
  const commands = commandText.split('\n').filter(command => command !== '');
  let gameData = {
    teams: [],
    turnTeamId: 0,
  };
  const resultsList = commands.map(command => {
    const commandComponents = command.split(' ');
    const verb = commandComponents[0];
    if (verb === 'CREATE') {
      const create = createMonpokeOnTeam(commandComponents, gameData);
      gameData = create.gameData;
      return create.result;
    }
    if (verb === 'ICHOOSEYOU') {
      const iChooseYou = chooseFighter(commandComponents, gameData);
      gameData = iChooseYou.gameData;
      return iChooseYou.result;
    }
    if (verb === 'ATTACK') {
      const attack = attackEnemy(gameData);
      gameData = attack.gameData;
      return attack.result;
    }
    if (verb === 'HEAL') {
      const heal = healFighter(commandComponents, gameData);
      gameData = heal.gameData;
      return heal.result;
    }
    if (verb === 'REVIVE') {
      const revive = reviveFighter(commandComponents, gameData);
      gameData = revive.gameData;
      return revive.result;
    }
    return error();
  });
  return resultsList.join('\n');
};
