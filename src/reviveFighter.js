const error = require('./error');

// eslint-disable-next-line no-unused-vars
module.exports = ([commandName, monpokeName], gameData) => {
  if (gameData.teams.length < 2) return error();

  const currentTeam = gameData.teams[gameData.turnTeamId];
  if (!currentTeam.extraLife) return error();

  const currentTeamMonpokeList = currentTeam.monpokeList;
  const selectedMonpokeIndex = currentTeamMonpokeList.findIndex(
    monpoke => monpoke.name === monpokeName,
  );
  const currentMonpoke = currentTeamMonpokeList[selectedMonpokeIndex];

  if (!currentMonpoke || currentMonpoke.monpokeId === -1 || currentMonpoke.hp > 0) {
    return error();
  }

  currentMonpoke.hp = currentMonpoke.maxHp;
  const anotherMonpokeActive = currentTeamMonpokeList.find(monpoke => monpoke.inBattle);
  if (!anotherMonpokeActive) currentMonpoke.inBattle = true;

  currentTeamMonpokeList[selectedMonpokeIndex] = currentMonpoke;
  /* eslint-disable no-param-reassign */
  gameData.teams[gameData.turnTeamId].extraLife = false;
  gameData.teams[gameData.turnTeamId].monpokeList = currentTeamMonpokeList;
  gameData.turnTeamId = gameData.turnTeamId === 0 ? 1 : 0;
  /* eslint-enable no-param-reassign */

  return {
    gameData,
    result: `${currentMonpoke.name} has been revived`,
  };
};
