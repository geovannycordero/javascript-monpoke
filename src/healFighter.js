const error = require('./error');

// eslint-disable-next-line no-unused-vars
module.exports = ([commandName, healAmount], gameData) => {
  if (gameData.teams.length < 2) return error();

  const currentTeamMonpokeList = gameData.teams[gameData.turnTeamId].monpokeList;
  const currentMonpoke = currentTeamMonpokeList[
    currentTeamMonpokeList.findIndex(monpoke => monpoke.inBattle)
  ];

  if (currentMonpoke && (currentMonpoke.monpokeId === -1 || currentMonpoke.hp < 1)) {
    return error();
  }

  let healedAmount = currentMonpoke.maxHp - currentMonpoke.hp;
  if (Number(healAmount) > currentMonpoke.maxHp) {
    currentMonpoke.hp = currentMonpoke.maxHp;
  } else {
    const amountToHeal = Number(healAmount) + currentMonpoke.hp;

    healedAmount = amountToHeal > currentMonpoke.maxHp
      ? currentMonpoke.maxHp - currentMonpoke.hp : amountToHeal - currentMonpoke.hp;

    currentMonpoke.hp = amountToHeal > currentMonpoke.maxHp ? currentMonpoke.maxHp : amountToHeal;
  }

  currentTeamMonpokeList[
    currentTeamMonpokeList.findIndex(monpoke => monpoke.inBattle)
  ] = currentMonpoke;

  /* eslint-disable no-param-reassign */
  gameData.teams[gameData.turnTeamId].monpokeList = currentTeamMonpokeList;
  gameData.turnTeamId = gameData.turnTeamId === 0 ? 1 : 0;
  /* eslint-enable no-param-reassign */

  return {
    gameData,
    result: `${currentMonpoke.name} healed for ${healedAmount} to ${currentMonpoke.hp}`,
  };
};
