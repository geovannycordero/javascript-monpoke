Implement the following additional features:

Heal
- Heal command is structured like attack `HEAL <HEALAMOUNT>`
- Heal cannot be executed until both teams have chosen a monpoke (exit 1)
- Heal ends the turn
- Heal amount can not exceed the initial HP of the monpoke
- You cannot heal a monpoke with a current HP of less than 1 (exit 1)
- Heal is applied to the currently selected Monpoke
- Heal should output `"<MONPOKE> healed for <HEALAMOUNT> to <NEWHEALTH>"`
    - <HEALAMOUNT> should reflect the amount actually healed.
        - For example, if a monpoke has 10 max HP and 8 current HP, and you try to heal for 4 HP, HEALAMOUNT should only be 2, since that is the actual amount of HP that has been healed.

Revive
- Revive command is structured as `REVIVE <MONPOKE NAME>`
- Revive ends the turn
- Each team only gets one Revive per match (exit 1 if used more than once)
- You cannot revive a monpoke with a health > 0 (exit 1)
- Revive should output `"<Monpoke name> has been revived"`
- Revive will set your current Monpoke as the selected one if no others are currently selected


Feel free to refactor the code in any way that you think is best. You can just drop a link to a clone or fork of the repository when you are ready for me to review. Cheers!
