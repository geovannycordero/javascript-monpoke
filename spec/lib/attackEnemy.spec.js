const attackEnemy = require('../../src/attackEnemy');

describe('attackEnemy', () => {
  it('can fight currently selected monpoke and enemy', () => {
    let gameData = {
      teams: [
        {
          name: 'Rocket',
          extraLife: true,
          monpokeList: [
            { name: 'Meekachu', hp: 3, maxHp: 3, ap: 1, inBattle: true },
          ],
        },
        {
          name: 'Green',
          extraLife: true,
          monpokeList: [
            { name: 'Smorelax', hp: 2, maxHp: 2, ap: 1, inBattle: true },
          ],
        },
      ],
      turnTeamId: 0,
    };
    const commands = [
      ['ATTACK'],
      ['ATTACK'],
    ];

    commands.forEach(() => {
      const result = attackEnemy(gameData);
      gameData = result.gameData;
    });

    expect(gameData.turnTeamId).toBe(0);

    const meekachu = gameData.teams[0].monpokeList[0];
    const smorelax = gameData.teams[1].monpokeList[0];

    expect(meekachu.hp).toEqual(3 - smorelax.ap);
    expect(smorelax.hp).toEqual(2 - meekachu.ap);
  });

  it('errors on a fight with monpoke with hp less than 1', () => {
    const gameData = {
      teams: [
        {
          name: 'Rocket',
          extraLife: true,
          monpokeList: [
            { name: 'Meekachu', hp: 0, maxHp: 3, ap: 1, inBattle: true },
          ],
        },
        {
          name: 'Green',
          extraLife: true,
          monpokeList: [
            { name: 'Smorelax', hp: 2, maxHp: 2, ap: 1, inBattle: true },
          ],
        },
      ],
      turnTeamId: 0,
    };

    const result = attackEnemy(gameData);

    expect(result).toBe('process.exit(1)');
  });

  it('errors on a fight if there is only 1 team', () => {
    const gameData = {
      teams: [
        {
          name: 'Rocket',
          extraLife: true,
          monpokeList: [
            { name: 'Meekachu', hp: 1, maxHp: 3, ap: 5, inBattle: true },
          ],
        },
      ],
      turnTeamId: 0,
    };

    const result = attackEnemy(gameData);

    expect(result).toBe('process.exit(1)');
  });
});
