const chooseFighter = require('../../src/chooseFighter');

describe('chooseFighter', () => {
  let gameData;

  beforeEach(() => {
    gameData = {
      teams: [
        {
          name: 'Rocket',
          extraLife: true,
          monpokeList: [
            {
              name: 'Meekachu',
              hp: 3,
              maxHp: 3,
              ap: 1,
              inBattle: false,
            },
            {
              name: 'Clefamous',
              hp: 3,
              maxHp: 3,
              ap: 6,
              inBattle: false,
            },
            {
              name: 'Rastly',
              hp: 5,
              maxHp: 5,
              ap: 6,
              inBattle: false,
            },
          ],
        },
        {
          name: 'Green',
          extraLife: true,
          monpokeList: [
            {
              name: 'Smorelax',
              hp: 2,
              maxHp: 2,
              ap: 1,
              inBattle: false,
            },
            {
              name: 'Vulsaur',
              hp: 4,
              maxHp: 4,
              ap: 2,
              inBattle: false,
            },
            {
              name: 'Bulbatail',
              hp: 3,
              maxHp: 3,
              ap: 2,
              inBattle: false,
            },
          ],
        },
      ],
      turnTeamId: 0,
    };
  });

  it('can select a monpoke fighter from each team', () => {
    const commands = [
      'ICHOOSEYOU Meekachu'.split(' '),
      'ICHOOSEYOU Vulsaur'.split(' '),
    ];

    commands.forEach(command => {
      const result = chooseFighter(command, gameData);
      gameData = result.gameData;
    });

    expect(gameData.turnTeamId).toBe(0);
    expect(gameData.teams[0].monpokeList[0].inBattle).toBe(true);
    expect(gameData.teams[0].monpokeList[1].inBattle).toBe(false);
    expect(gameData.teams[0].monpokeList[2].inBattle).toBe(false);

    expect(gameData.teams[1].monpokeList[0].inBattle).toBe(false);
    expect(gameData.teams[1].monpokeList[1].inBattle).toBe(true);
    expect(gameData.teams[1].monpokeList[2].inBattle).toBe(false);
  });

  it('cannot choose a monpoke not on your team', () => {
    const command = 'ICHOOSEYOU Smorelax'.split(' ');

    const result = chooseFighter(command, gameData);
    expect(result).toBe('process.exit(1)');
  });

  it('cannot choose a monpoke who has been defeated', () => {
    gameData = {
      teams: [
        {
          name: 'Rocket',
          extraLife: true,
          monpokeList: [
            {
              name: 'Meekachu',
              hp: 1,
              maxHp: 1,
              ap: 5,
              inBattle: false,
            },
          ],
        },
        {
          name: 'Team2',
          extraLife: true,
          monpokeList: [
            {
              name: 'Meekachu',
              hp: 0,
              maxHp: 3,
              ap: 2,
              inBattle: false,
            },
          ],
        },
      ],
      turnTeamId: 1,
    };
    const command = 'ICHOOSEYOU Meekachu'.split(' ');

    const result = chooseFighter(command, gameData);
    expect(result).toBe('process.exit(1)');
  });

  it('errors if there are less than 2 teams', () => {
    gameData = {
      teams: [
        {
          name: 'Rocket',
          extraLife: true,
          monpokeList: [
            {
              name: 'Meekachu',
              hp: 1,
              maxHp: 1,
              ap: 5,
              inBattle: false,
            },
          ],
        },
      ],
      turnTeamId: 0,
    };

    const command = ['ICHOOSEYOU Meekachu'.split(' ')];

    const result = chooseFighter(command, gameData);
    expect(result).toBe('process.exit(1)');
  });
});
