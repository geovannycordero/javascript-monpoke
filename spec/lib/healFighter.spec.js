const healFighter = require('../../src/healFighter');

describe('healFighter', () => {
  it('errors out healing a fighter if there is only 1 team', () => {
    const gameData = {
      teams: [
        {
          name: 'Rocket',
          extraLife: true,
          monpokeList: [
            { name: 'Meekachu', hp: 1, maxHp: 1, ap: 5, inBattle: true },
          ],
        },
      ],
      turnTeamId: 0,
    };

    const healFighterCommand = 'HEAL 1'.split(' ');
    const result = healFighter(healFighterCommand, gameData);

    expect(result).toBe('process.exit(1)');
  });

  it('errors out healing a fighter that has the hp < 0', () => {
    const gameData = {
      teams: [
        {
          name: 'Rocket',
          extraLife: true,
          monpokeList: [
            { name: 'Meekachu', hp: 1, maxHp: 1, ap: 5, inBattle: true },
          ],
        },
        {
          name: 'Green',
          extraLife: true,
          monpokeList: [
            { name: 'Smorelax', hp: 0, maxHp: 1, ap: 1, inBattle: true },
          ],
        },
      ],
      turnTeamId: 1,
    };

    const healFighterCommand = 'HEAL 3'.split(' ');
    const result = healFighter(healFighterCommand, gameData);

    expect(result).toBe('process.exit(1)');
  });

  it('heals the fighter HP', () => {
    const gameData = {
      teams: [
        {
          name: 'Rocket',
          extraLife: true,
          monpokeList: [
            { name: 'Meekachu', hp: 1, maxHp: 3, ap: 1, inBattle: true },
          ],
        },
        {
          name: 'Green',
          extraLife: true,
          monpokeList: [
            { name: 'Smorelax', hp: 1, maxHp: 2, ap: 1, inBattle: true },
          ],
        },
      ],
      turnTeamId: 1,
    };

    const healFighterCommand = 'HEAL 1'.split(' ');
    healFighter(healFighterCommand, gameData);

    expect(gameData.turnTeamId).toBe(0);

    const team = gameData.teams[1];
    expect(team.monpokeList[0])
      .toEqual({
        name: 'Smorelax',
        hp: 2,
        maxHp: 2,
        ap: 1,
        inBattle: true,
      });
  });

  it('heals only the HP of the selected fighter', () => {
    const gameData = {
      teams: [
        {
          name: 'Rocket',
          extraLife: true,
          monpokeList: [
            { name: 'Meekachu', hp: 1, maxHp: 3, ap: 2, inBattle: true },
          ],
        },
        {
          name: 'Green',
          extraLife: true,
          monpokeList: [
            { name: 'Smorelax', hp: 1, maxHp: 2, ap: 1, inBattle: true },
          ],
        },
      ],
      turnTeamId: 1,
    };

    const healFighterCommand = 'HEAL 2'.split(' ');
    healFighter(healFighterCommand, gameData);

    const teamOne = gameData.teams[0];
    expect(teamOne.monpokeList[0])
      .toEqual({
        name: 'Meekachu',
        hp: 1,
        maxHp: 3,
        ap: 2,
        inBattle: true,
      });

    const teamTwo = gameData.teams[1];
    expect(teamTwo.monpokeList[0])
      .toEqual({
        name: 'Smorelax',
        hp: 2,
        maxHp: 2,
        ap: 1,
        inBattle: true,
      });
  });

  it('can heals only up to the initial HP of the selected monpoke', () => {
    const gameData = {
      teams: [
        {
          name: 'Rocket',
          extraLife: true,
          monpokeList: [
            { name: 'Meekachu', hp: 3, maxHp: 7, ap: 1, inBattle: true },
          ],
        },
        {
          name: 'Green',
          extraLife: true,
          monpokeList: [
            { name: 'Smorelax', hp: 1, maxHp: 2, ap: 1, inBattle: true },
          ],
        },
      ],
      turnTeamId: 0,
    };

    const healFighterCommand = 'HEAL 6'.split(' ');
    healFighter(healFighterCommand, gameData);

    const team = gameData.teams[0];
    expect(team.monpokeList[0])
      .toEqual({
        name: 'Meekachu',
        hp: 7,
        maxHp: 7,
        ap: 1,
        inBattle: true,
      });
  });

  it('returns the right output after healing the fighter HP, with values less that the max hp', () => {
    const gameData = {
      teams: [
        {
          name: 'Rocket',
          extraLife: true,
          monpokeList: [
            { name: 'Meekachu', hp: 3, maxHp: 13, ap: 1, inBattle: true },
          ],
        },
        {
          name: 'Green',
          extraLife: true,
          monpokeList: [
            { name: 'Smorelax', hp: 1, maxHp: 2, ap: 1, inBattle: true },
          ],
        },
      ],
      turnTeamId: 0,
    };

    const healFighterCommand = 'HEAL 5'.split(' ');
    const response = healFighter(healFighterCommand, gameData);

    expect(response.result).toBe('Meekachu healed for 5 to 8');

    const team = gameData.teams[0];
    expect(team.monpokeList[0])
      .toEqual({
        name: 'Meekachu',
        hp: 8,
        maxHp: 13,
        ap: 1,
        inBattle: true,
      });
  });

  it('returns the right output after healing the fighter HP, with values greater that the max hp', () => {
    const gameData = {
      teams: [
        {
          name: 'Rocket',
          extraLife: true,
          monpokeList: [
            { name: 'Meekachu', hp: 4, maxHp: 13, ap: 1, inBattle: true },
          ],
        },
        {
          name: 'Green',
          extraLife: true,
          monpokeList: [
            { name: 'Smorelax', hp: 1, maxHp: 2, ap: 1, inBattle: true },
          ],
        },
      ],
      turnTeamId: 0,
    };

    const healFighterCommand = 'HEAL 16'.split(' ');
    const response = healFighter(healFighterCommand, gameData);

    expect(response.result).toBe('Meekachu healed for 9 to 13');

    const team = gameData.teams[0];
    expect(team.monpokeList[0])
      .toEqual({
        name: 'Meekachu',
        hp: 13,
        maxHp: 13,
        ap: 1,
        inBattle: true,
      });
  });
});
