const createMonpokeOnTeam = require('../../src/createMonpokeOnTeam');

describe('createMonpokeOnTeam', () => {
  let gameData;

  beforeEach(() => {
    gameData = {
      teams: [],
      turnTeamId: 0,
    };
  });

  it('can create a team with monpoke name, hp, ap, and if in battle', () => {
    const createTeamCommand = 'CREATE Rocket Meekachu 3 1'.split(' ');
    const createTeam = createMonpokeOnTeam(createTeamCommand, gameData);
    gameData = createTeam.gameData;

    expect(gameData.teams.length)
      .toBe(1);
    const team = gameData.teams[0];
    expect(team.name)
      .toBe('Rocket');
    expect(team.extraLife)
      .toBe(true);
    expect(team.monpokeList[0])
      .toEqual({
        name: 'Meekachu',
        hp: 3,
        maxHp: 3,
        ap: 1,
        inBattle: false,
      });
  });

  it('errors out if monpoke has < 1 ap', () => {
    const createTeamCommand = 'CREATE Rocket Meekachu 3 0'.split(' ');

    const result = createMonpokeOnTeam(createTeamCommand, gameData);

    expect(result)
      .toBe('process.exit(1)');
  });

  it('errors out if monpoke has < 1 hp', () => {
    const createTeamCommand = 'CREATE Rocket Meekachu 0 3'.split(' ');

    const result = createMonpokeOnTeam(createTeamCommand, gameData);

    expect(result)
      .toBe('process.exit(1)');
  });

  it('can create a team with multiple monpoke', () => {
    const commands = [
      'CREATE Rocket Meekachu 3 1',
      'CREATE Rocket Clefamous 3 6',
      'CREATE Green Smorelax 2 1',
      'CREATE Rocket Rastly 5 6',
      'CREATE Green Vulsaur 4 2',
      'CREATE Green Bulbatail 3 2',
    ].map(c => c.split(' '));

    commands.forEach(command => {
      const result = createMonpokeOnTeam(command, gameData);
      gameData = result.gameData;
    });

    expect(gameData.teams.length).toBe(2);
    expect(gameData.teams[0].name).toBe('Rocket');
    expect(gameData.teams[1].name).toBe('Green');

    expect(gameData.teams[0].monpokeList.length).toBe(3);
    expect(gameData.teams[1].monpokeList.length).toBe(3);

    expect(gameData.teams[0].monpokeList[0].name).toBe('Meekachu');
    expect(gameData.teams[0].monpokeList[1].name).toBe('Clefamous');
    expect(gameData.teams[0].monpokeList[2].name).toBe('Rastly');

    expect(gameData.teams[1].monpokeList[0].name).toBe('Smorelax');
    expect(gameData.teams[1].monpokeList[1].name).toBe('Vulsaur');
    expect(gameData.teams[1].monpokeList[2].name).toBe('Bulbatail');
  });
});
