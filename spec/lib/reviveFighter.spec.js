const reviveFighter = require('../../src/reviveFighter');

describe('reviveFighter', () => {
  it('errors out reviving a fighter if there is only 1 team', () => {
    const gameData = {
      teams: [
        {
          name: 'Rocket',
          extraLife: true,
          monpokeList: [
            { name: 'Meekachu', hp: 0, maxHp: 2, ap: 5, inBattle: false },
          ],
        },
      ],
      turnTeamId: 0,
    };

    const healFighterCommand = 'REVIVE Meekachu'.split(' ');
    const result = reviveFighter(healFighterCommand, gameData);

    expect(result).toBe('process.exit(1)');
  });

  it('can not revives a fighter with the hp > 0', () => {
    const gameData = {
      teams: [
        {
          name: 'Rocket',
          extraLife: true,
          monpokeList: [
            { name: 'Meekachu', hp: 1, maxHp: 3, ap: 2, inBattle: true },
          ],
        },
        {
          name: 'Green',
          extraLife: true,
          monpokeList: [
            { name: 'Smorelax', hp: 3, maxHp: 5, ap: 1, inBattle: true },
          ],
        },
      ],
      turnTeamId: 1,
    };

    const healFighterCommand = 'REVIVE Smorelax'.split(' ');
    const result = reviveFighter(healFighterCommand, gameData);

    expect(result).toBe('process.exit(1)');
  });

  it('can not revives a fighter that is in another team', () => {
    const gameData = {
      teams: [
        {
          name: 'Rocket',
          extraLife: true,
          monpokeList: [
            { name: 'Meekachu', hp: 0, maxHp: 3, ap: 2, inBattle: false },
          ],
        },
        {
          name: 'Green',
          extraLife: true,
          monpokeList: [
            { name: 'Smorelax', hp: 3, maxHp: 5, ap: 1, inBattle: true },
          ],
        },
      ],
      turnTeamId: 1,
    };

    const healFighterCommand = 'REVIVE Meekachu'.split(' ');
    const result = reviveFighter(healFighterCommand, gameData);

    expect(result).toBe('process.exit(1)');
  });

  it('can revives a fighter', () => {
    const gameData = {
      teams: [
        {
          name: 'Rocket',
          extraLife: true,
          monpokeList: [
            { name: 'Meekachu', hp: 2, maxHp: 3, ap: 7, inBattle: true },
          ],
        },
        {
          name: 'Green',
          extraLife: true,
          monpokeList: [
            { name: 'Smorelax', hp: 0, maxHp: 5, ap: 2, inBattle: false },
            { name: 'Absol', hp: 1, maxHp: 3, ap: 1, inBattle: true },
          ],
        },
      ],
      turnTeamId: 1,
    };

    const healFighterCommand = 'REVIVE Smorelax'.split(' ');
    const response = reviveFighter(healFighterCommand, gameData);

    expect(gameData.turnTeamId).toBe(0);
    expect(response.result).toBe('Smorelax has been revived');

    const team = gameData.teams[1];
    expect(team.monpokeList[0])
      .toEqual({
        name: 'Smorelax',
        hp: 5,
        maxHp: 5,
        ap: 2,
        inBattle: false,
      });
    expect(team.monpokeList[1])
      .toEqual({
        name: 'Absol',
        hp: 1,
        maxHp: 3,
        ap: 1,
        inBattle: true,
      });
  });

  it('can revives only one fighter per match', () => {
    const gameData = {
      teams: [
        {
          name: 'Rocket',
          extraLife: true,
          monpokeList: [
            { name: 'Meekachu', hp: 0, maxHp: 3, ap: 2, inBattle: false },
            { name: 'Jolteon', hp: 0, maxHp: 3, ap: 2, inBattle: false },
          ],
        },
        {
          name: 'Green',
          extraLife: true,
          monpokeList: [
            { name: 'Smorelax', hp: 0, maxHp: 5, ap: 2, inBattle: false },
            { name: 'Absol', hp: 3, maxHp: 5, ap: 2, inBattle: true },
          ],
        },
      ],
      turnTeamId: 0,
    };

    let healFighterCommand = 'REVIVE Meekachu'.split(' ');
    let response = reviveFighter(healFighterCommand, gameData);

    expect(gameData.turnTeamId).toBe(1);
    expect(response.result).toBe('Meekachu has been revived');

    const team = gameData.teams[0];
    expect(team.monpokeList[0])
      .toEqual({
        name: 'Meekachu',
        hp: 3,
        maxHp: 3,
        ap: 2,
        inBattle: true,
      });

    gameData.turnTeamId = 0;
    healFighterCommand = 'REVIVE Jolteon'.split(' ');
    response = reviveFighter(healFighterCommand, gameData);

    expect(response).toBe('process.exit(1)');
  });

  it('can not activate the current revived Monpoke if there is another one active', () => {
    const gameData = {
      teams: [
        {
          name: 'Rocket',
          extraLife: true,
          monpokeList: [
            { name: 'Meekachu', hp: 0, maxHp: 3, ap: 2, inBattle: false },
            { name: 'Jolteon', hp: 1, maxHp: 3, ap: 2, inBattle: true },
          ],
        },
        {
          name: 'Green',
          extraLife: true,
          monpokeList: [
            { name: 'Smorelax', hp: 0, maxHp: 5, ap: 2, inBattle: false },
            { name: 'Absol', hp: 3, maxHp: 5, ap: 2, inBattle: true },
          ],
        },
      ],
      turnTeamId: 0,
    };

    const healFighterCommand = 'REVIVE Meekachu'.split(' ');
    const response = reviveFighter(healFighterCommand, gameData);

    expect(gameData.turnTeamId).toBe(1);
    expect(response.result).toBe('Meekachu has been revived');

    const team = gameData.teams[0];
    expect(team.monpokeList[0])
      .toEqual({
        name: 'Meekachu',
        hp: 3,
        maxHp: 3,
        ap: 2,
        inBattle: false,
      });
  });

  it('can activate the current revived Monpoke if there is none active', () => {
    const gameData = {
      teams: [
        {
          name: 'Rocket',
          extraLife: true,
          monpokeList: [
            { name: 'Meekachu', hp: 0, maxHp: 3, ap: 2, inBattle: false },
            { name: 'Jolteon', hp: 1, maxHp: 3, ap: 2, inBattle: false },
          ],
        },
        {
          name: 'Green',
          extraLife: true,
          monpokeList: [
            { name: 'Smorelax', hp: 0, maxHp: 5, ap: 2, inBattle: false },
            { name: 'Absol', hp: 3, maxHp: 5, ap: 2, inBattle: true },
          ],
        },
      ],
      turnTeamId: 0,
    };

    const healFighterCommand = 'REVIVE Meekachu'.split(' ');
    const response = reviveFighter(healFighterCommand, gameData);

    expect(gameData.turnTeamId).toBe(1);
    expect(response.result).toBe('Meekachu has been revived');

    const team = gameData.teams[0];
    expect(team.monpokeList[0])
      .toEqual({
        name: 'Meekachu',
        hp: 3,
        maxHp: 3,
        ap: 2,
        inBattle: true,
      });
  });
});
