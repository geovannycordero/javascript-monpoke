const createGame = require('../../src/game');

describe('User can play game', () => {
  it('and create teams with monpoke, and choose attackers', () => {
    const createCommands = 'CREATE Rocket Meekachu 2 1\nCREATE Rocket Rastly 5 6\nCREATE Green Smorelax 2 3\n';
    const round1Commands = 'ICHOOSEYOU Meekachu\nICHOOSEYOU Smorelax\nATTACK\nATTACK\n';

    const round2Commands = 'ICHOOSEYOU Rastly\nATTACK\nREVIVE Meekachu\nHEAL 6\n';
    const round3Commands = 'HEAL 9\nATTACK\nICHOOSEYOU Meekachu\nATTACK\n';
    const round4Commands = 'ICHOOSEYOU Rastly\nHEAL 1\nHEAL 1\nATTACK\n';

    const commands = `${createCommands}${round1Commands}${round2Commands}${round3Commands}${round4Commands}`;
    const gameOutput = createGame(commands);

    const createResults = 'Meekachu has been assigned to team Rocket!\nRastly has been assigned to team Rocket!\nSmorelax has been assigned to team Green!\n';

    const round1Results = 'Meekachu has entered the battle!\nSmorelax has entered the battle!\nMeekachu attacked Smorelax for 1 damage!\nSmorelax attacked Meekachu for 3 damage!\n';
    const round2Results = 'Rastly has entered the battle!\nSmorelax attacked Rastly for 3 damage!\nMeekachu has been revived\nSmorelax healed for 1 to 2\n';
    const round3Results = 'Rastly healed for 3 to 5\nSmorelax attacked Rastly for 3 damage!\nMeekachu has entered the battle!\nSmorelax attacked Meekachu for 3 damage!\n';
    const round4Results = 'Rastly has entered the battle!\nSmorelax healed for 0 to 2\nRastly healed for 1 to 3\nSmorelax attacked Rastly for 3 damage!\n';

    const battleResults = 'Rastly has been defeated!\nGreen is the winner!';

    const expectedOutput = `${createResults}${round1Results}${round2Results}${round3Results}${round4Results}${battleResults}`;
    expect(gameOutput).toBe(expectedOutput);
  });

  it('will error out with malformed command verb', () => {
    const commands = 'OHNO Rocket Meekachu 3 1';
    const gameOutput = createGame(commands);

    expect(gameOutput).toBe('process.exit(1)');
  });
});
